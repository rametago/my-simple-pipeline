## Build stage

Use virtualenv to build python environment: virtualenv WORKDIR/env

Activate the environment: source env/bin/activate

Install requirements: pip install -r requirements.txt



## Test stage

Run tests: python manage.py test

Check the server:

nohup python manage.py runserver &
sleep(10)
curl -sSf http://localhost:8080 > /dev/null && echo "Up" | grep "Up"
pkill python
rm nohup.out




## Release stage

Login to Gitlab registry: docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

Build docker image: docker build -t $CONTAINER_IMAGE .

Push image to Gitlab registry: docker push $CONTAINER_IMAGE

Logout: docker logout



Deploy

Connect to webserver and run docker container: ssh -o StrictHostKeyChecking=no -i $SSH_KEY user@webserver_ip docker run -d -p 8000:8000 $CONTAINER_IMAGE
